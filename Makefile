CC= gcc

CFLAGS_STATIC= -Wall -Wextra -fdiagnostics-color=always -Os
CFLAGS_SHARED= $(CFLAGS_STATIC) -fPIC

BIN= liblocalfs.a liblocalfs.so

SRC= localfs.c
STATICOBJ= $(patsubst %.c, static/%.o, $(SRC))
SHAREDOBJ= $(patsubst %.c, shared/%.o, $(SRC))


.PHONY: all clean dist dist-clean

all: localfs

localfs: $(BIN)
	mkdir -p localfs
	mkdir -p localfs/include
	mkdir -p localfs/lib
	cp localfs.h localfs/include/.
	mv *.so* localfs/lib/.
	mv *.a localfs/lib/.

shared/%.o: %.c
	-@mkdir shared
	$(CC) $(CFLAGS_SHARED) -c -o$@ $<

static/%.o: %.c
	-@mkdir static
	$(CC) $(CFLAGS_STATIC) -c -o$@ $<

liblocalfs.a: $(STATICOBJ)
	ar -cvq $@ $^

liblocalfs.so: $(SHAREDOBJ)
	$(CC) -shared -Wl,-soname,$@.1 -o $@.1.0 $^
	ln -sf $@.1.0 $@.1
	ln -sf $@.1.0 $@

dist: localfs
	mkdir -p localfs/doc
	ln -sf -T LICENSE.md COPYING
	cp README.md localfs/doc
	cp LICENSE.md localfs/doc
	mv COPYING localfs/doc
	mkdir -p release
	tar -cz localfs > release/localfs.tar.gz
	tar -cj localfs > release/localfs.tar.bz2
	tar -cJ localfs > release/localfs.tar.xz

clean:
	-$(RM) *.so* *.a
	-$(RM) -r localfs shared static

dist-clean: clean
	-$(RM) -r release

