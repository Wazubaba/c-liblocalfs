#ifndef LOCALFS_H_MPTQJ1SF
#define LOCALFS_H_MPTQJ1SF

/*
	liblfs - Library for working with localised xdg paths
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	This attempts to serve as an abstraction over user-level
	configuration. It will contain wrappers for constructing
	local paths with XDG support.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <linux/limits.h>
#include <unistd.h>
#include <signal.h>

/**
	Contains paths to various locations referenced by XDG, as well as
	other useful information.
*/
struct LFSPathMap
{
	char* runDirectory;
	char* username;
	char* home;
	char* config;
	char* data;
	char* cache;
	char* runtime;
	char* projectDir;
};

/**
	Initialise an LFSPathMap struct.
	Does not allocate, so no need to free anything.
*/
void lfs_pathmap_init(struct LFSPathMap* pmap, char* projectName);

/**
	All of the following functions map a path to be within a given
	directory from the LFSPathMap struct.

	The suffix _n denotes that the function does not allocate heap,
	copying the result into the provided buffer instead.

	The rest allocate a new string and return it.
**/

char* lfs_resolve_config(const struct LFSPathMap* pmap, const char* fileName);
void lfs_resolve_config_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName);
char* lfs_resolve_data(const struct LFSPathMap* pmap, const char* fileName);
void lfs_resolve_data_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName);
char* lfs_resolve_cache(const struct LFSPathMap* pmap, const char* fileName);
void lfs_resolve_cache_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName);
char* lfs_resolve_runtime(const struct LFSPathMap* pmap, const char* fileName);
void lfs_resolve_runtime_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName);

/**
	Open a file handle in the given directory from the LFSPathMap struct
	and return it.
*/

FILE* lfs_open_config(struct LFSPathMap* pmap, char* fpath, char* mode);
FILE* lfs_open_data(struct LFSPathMap* pmap, char* fpath, char* mode);
FILE* lfs_open_cache(struct LFSPathMap* pmap, char* fpath, char* mode);
FILE* lfs_open_runtime(struct LFSPathMap* pmap, char* fpath, char* mode);

/**
	Helper to close the file if stdio.h cannot be imported for whatever
	reason...
*/
void lfs_closeFile(FILE* fp);

#endif /* end of include guard: LOCALFS_H_MPTQJ1SF */

