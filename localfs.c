/*
	liblfs - Library for working with localised xdg paths
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "localfs.h"

void lfs_pathmap_init(struct LFSPathMap* pmap, char* projectName)
{
	char* user = getenv("USER");
	strcpy(pmap->username, user);
	strcpy(pmap->projectDir, projectName);

	char* temp;
	temp = getcwd(pmap->runDirectory, PATH_MAX);
	
	if (temp == NULL || strncmp(pmap->username, "root", 4) == 0)
	{
		strcpy(pmap->config, "/etc");
		strcpy(pmap->cache, "/var/cache");
		strcpy(pmap->data, "/usr/local/share");
		strcpy(pmap->runtime, "/tmp");
		return;
	}
	
	char* xdg_val = NULL;

	char* envMap[4] = {"XDG_CONFIG_HOME", "XDG_CACHE_HOME", "XDG_DATA_HOME", "XDG_RUNTIME_DIR"};
	char* envOverrides[4] = {".config", ".cache", ".local/share", "/tmp"};

	size_t i;
	for (i = 0; i < 4; i++)
	{
		xdg_val = getenv(envMap[i]);
		if (xdg_val == NULL)
			sprintf(pmap->config, "/home/%s/%s/%s", pmap->username, envOverrides[i], pmap->projectDir);
		else
		{
			char temp[PATH_MAX];
			sprintf(temp, "%s/%s", xdg_val, pmap->projectDir);
			switch (i)
			{
				case 0:
					strcpy(pmap->config, temp); break;
				case 1:
					strcpy(pmap->cache, temp); break;
				case 2:
					strcpy(pmap->data, temp); break;
				case 3:
					strcpy(pmap->runtime, temp); break;
				default:
					raise(SIGSEGV); break;
			}
		}

		free(xdg_val);
	}
}


inline char* resolve(const char* path, const char* fileName)
{
	char* rhs = malloc(sizeof(char) * PATH_MAX);
	sprintf(rhs, "%s/%s", path, fileName);

	return rhs;
}

inline void resolve_n(char* buffer, const char* path, const char* fileName)
{
	size_t bmax = strlen(buffer) - 1;
	snprintf(buffer, bmax, "%s/%s", path, fileName);
}


char* lfs_resolve_config(const struct LFSPathMap* pmap, const char* fileName)
{
	return resolve(pmap->config, fileName);
}

void lfs_resolve_config_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName)
{
	resolve_n(buffer, pmap->config, fileName);
}

char* lfs_resolve_data(const struct LFSPathMap* pmap, const char* fileName)
{
	return resolve(pmap->data, fileName);
}

void lfs_resolve_data_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName)
{
	resolve_n(buffer, pmap->data, fileName);
}

char* lfs_resolve_cache(const struct LFSPathMap* pmap, const char* fileName)
{
	return resolve(pmap->cache, fileName);
}

void lfs_resolve_cache_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName)
{
	resolve_n(buffer, pmap->cache, fileName);
}

char* lfs_resolve_runtime(const struct LFSPathMap* pmap, const char* fileName)
{
	return resolve(pmap->runtime, fileName);
}

void lfs_resolve_runtime_n(char* buffer, const struct LFSPathMap* pmap, const char* fileName)
{
	resolve_n(buffer, pmap->runtime, fileName);
}

FILE* lfs_open_config(struct LFSPathMap* pmap, char* fpath, char* mode)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", pmap->config, fpath);
	return fopen(path, mode);
}

FILE* lfs_open_data(struct LFSPathMap* pmap, char* fpath, char* mode)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", pmap->data, fpath);
	return fopen(path, mode);
}

FILE* lfs_open_cache(struct LFSPathMap* pmap, char* fpath, char* mode)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", pmap->cache, fpath);
	return fopen(path, mode);
}

FILE* lfs_open_runtime(struct LFSPathMap* pmap, char* fpath, char* mode)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", pmap->runtime, fpath);
	return fopen(path, mode);
}

void lfs_closeFile(FILE* fp)
{
	fclose(fp);
}

