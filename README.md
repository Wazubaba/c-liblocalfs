# L O C A L F S
A C library to help with file io that is meant to serve as
configuration or cached data (think stuff you'd normally throw into a
home folder).

This library contains numerous helpers for aquiring proper paths
following a lax version of the XDG standard. It supports the
XDGHOME-style environment variables but not the multi-path ones (yet).

The API is simple, small, and documented within the header.

